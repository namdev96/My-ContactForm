import React from 'react';


class ContactForm extends React.Component {
  constructor (props){
    super(props);
    this.initialState = { name:'',email:'',city:'', message:''};
    this.state = this.initialState;
    //this.onHandleSubmit = this.onHandleSubmit.bind(this);
    //this.onHandleChange = this.onHandleChange.bind(this);
 
  }
  onHandleChange = (e) =>{
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
     [name]:value
    });
    
    }
    onHandleSubmit = (e) =>{
      e.preventDefault()
      console.log(this.state);
    }
  
  render()
  {
    return (
      <div>
      <form onSubmit = {this.onHandleSubmit}>
      <h1>Contact From</h1>
      <div>
      Name:
      <input type = "text" name = "name" value = {this.state.name} onChange = {this.onHandleChange}/>
      </div>
      <div>
      Email:
      <input type = "email" name = "email" value = {this.state.email} onChange = {this.onHandleChange}/>
      </div>
      <div>
      City:
      <select value = {this.state.city} name = "city" onChange = {this.onHandleChange}>
      <option >--Select City--</option>
      <option >Delhi</option>
      <option >Gwalior</option>
      <option >Mumbai</option>
      <option >Agra</option>
      
      </select>
      </div>
      <div>
      Message:
      <textarea name = "message"  value={this.state.message} onChange = {this.onHandleChange}/>
      </div>
     
      <div>
          <button type = "submit" >Submit</button>
      </div>
      </form>
     </div>      
    )
  
}
}
export default ContactForm;